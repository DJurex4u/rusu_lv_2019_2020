Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.

UPDATE:
promijenjena funkcija s "inputx" na "input", popravljeno ime varijable sa "fnamex" na "fname" i dodane neke zagrade kod print funkcije.