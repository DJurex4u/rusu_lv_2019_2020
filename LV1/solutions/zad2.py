gradeFloat = float(input('Unesi ocijenu brojkom od 0-1 '))

if gradeFloat >= 0.9:
    gradeAlphabet = 'A'
elif (gradeFloat < 0.9) and (gradeFloat >= 0.8):
    gradeAlphabet = 'B'
elif (gradeFloat < 0.8) and (gradeFloat >= 0.7):
    gradeAlphabet = 'C'
elif (gradeFloat < 0.7) and (gradeFloat >= 0.6):
    gradeAlphabet = 'D'
else:
    gradeAlphabet = 'F'


print(gradeAlphabet)